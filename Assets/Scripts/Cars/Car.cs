﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Cinemachine;

public class Car : MonoBehaviour
{
    private CinemachineDollyCart dolly;
    private float raycastInterval;

    private float originalSpeed;

    private new Collider collider;
    private void Start() {
        dolly = GetComponent<CinemachineDollyCart>();
        originalSpeed = dolly.m_Speed;
        collider = GetComponent<Collider>();
    }

    private void Update() {
        if (dolly && dolly.m_Position >= dolly.m_Path.PathLength)
        {
            dolly.m_Position = 0;
        }

        if(Physics.BoxCast(collider.bounds.center, transform.localScale, transform.forward,transform.rotation, 3, LayerMask.GetMask("Traffic")))
        {
            dolly.m_Speed = 0;
        }else{
            dolly.m_Speed = originalSpeed;
        }
    }
}
