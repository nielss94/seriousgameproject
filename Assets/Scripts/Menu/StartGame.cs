﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;
using TMPro;
using UnityEngine.SceneManagement;

public class StartGame : MonoBehaviour, IPointerEnterHandler, IPointerExitHandler, IPointerClickHandler
{
    [SerializeField]
    private TextMeshProUGUI startGame;

    private Color startingColor;
    public Color highlightedColor;

    // Start is called before the first frame update
    void Start()
    {
        startGame = GetComponent<TextMeshProUGUI>();
        startingColor = startGame.color;
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void OnPointerEnter(PointerEventData eventData)
    {
        startGame.color = highlightedColor;
    }

    public void OnPointerExit(PointerEventData eventData)
    {
        startGame.color = startingColor;
    }

    public void OnPointerClick(PointerEventData eventData)
    {
        SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex + 1);
        Debug.Log("Starting the game...");
    }
}
