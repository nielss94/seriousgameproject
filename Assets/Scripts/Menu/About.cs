﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;
using TMPro;

public class About : MonoBehaviour, IPointerEnterHandler, IPointerExitHandler, IPointerClickHandler
{
    [SerializeField]
    private TextMeshProUGUI about;

    private Color startingColor;
    public Color highlightedColor;

    // Start is called before the first frame update
    void Start()
    {
        about = GetComponent<TextMeshProUGUI>();
        startingColor = about.color;
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void OnPointerEnter(PointerEventData eventData)
    {
        about.color = highlightedColor;
    }

    public void OnPointerExit(PointerEventData eventData)
    {
        about.color = startingColor;
    }

    public void OnPointerClick(PointerEventData eventData)
    {
        Debug.Log("Starting about Scene/Panel...");
    }
}
