﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using Object = UnityEngine.Object;

[RequireComponent(typeof(BounceUIAnimation))]
public class InventoryUI : MonoBehaviour
{
    [SerializeField] private GameObject inventoryList = null;
    [SerializeField] private GameObject inventoryItem = null;

    [Header("Selected buttons"), SerializeField]
    private GameObject selectedToolsButton = null;

    [SerializeField] private GameObject selectedNotesButton = null;

    private BounceUIAnimation bounceUIAnimation;
    private RectTransform inventoryTransform;

    private void Awake()
    {
        bounceUIAnimation = GetComponent<BounceUIAnimation>();
    }

    private void Start()
    {
        inventoryTransform = inventoryList.GetComponent<RectTransform>();
        DestroyAllInventoryItems();

        FillInventory(InventoryManager.instance.GetNotes().ToArray());
    }

    private void DestroyAllInventoryItems()
    {
        foreach (Transform child in inventoryTransform)
        {
            Destroy(child.gameObject);
        }
    }

    private void FillInventory(InventoryItem[] items)
    {
        foreach (InventoryItem item in items)
        {
            GameObject itemObject = Instantiate(inventoryItem, inventoryTransform.position,
                Quaternion.identity,
                inventoryTransform);
            itemObject.GetComponent<InventoryItemUI>().SetInventoryItem(item);
        }
    }

    public void ShowNotes()
    {
        Show(selectedNotesButton, selectedToolsButton, InventoryManager.instance.GetNotes().ToArray());
    }

    public void HideNotes()
    {
        Hide(selectedNotesButton);
    }

    public void ShowTools()
    {
        Show(selectedToolsButton, selectedNotesButton, InventoryManager.instance.GetTools().ToArray());
    }

    public void HideTools()
    {
        Hide(selectedToolsButton);
    }

    private void Show(GameObject selectButton, GameObject otherButton, InventoryItem[] items)
    {
        otherButton.SetActive(false);
        selectButton.SetActive(true);
        DestroyAllInventoryItems();
        FillInventory(items);
        bounceUIAnimation.InAnimation();
    }

    private void Hide(GameObject selectButton)
    {
        selectButton.SetActive(false);
        bounceUIAnimation.OutAnimation();
    }
}