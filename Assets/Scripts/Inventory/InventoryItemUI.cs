﻿using System;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class InventoryItemUI : MonoBehaviour
{
    [SerializeField] private Image inventoryImage = null;
    [SerializeField] private TextMeshProUGUI inventoryTitle = null;
    [SerializeField] private Button inventoryButton = null;

    private InventoryItem inventoryItem;
    private InventoryManager inventoryManager;

    private void Awake()
    {
        inventoryManager = FindObjectOfType<InventoryManager>();
    }

    private void Start()
    {
        inventoryButton.onClick.AddListener(Clicked);
    }

    public void SetInventoryItem(InventoryItem item)
    {
        inventoryItem = item;
        inventoryImage.sprite = item.image;
        inventoryTitle.text = item.title;
    }

    private void Clicked()
    {
        inventoryManager.ShowInventoryRead(inventoryItem);
    }
}