using System;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;
using DG.Tweening;

public class InventoryItemReadWindow : MonoBehaviour
{
    [SerializeField] private float fadeSpeed = .5f;

    [SerializeField] private Image itemImage = null;
    [SerializeField] private TextMeshProUGUI titleText = null;
    [SerializeField] private TextMeshProUGUI itemText = null;

    [SerializeField] private InventoryItem item = null;

    private CanvasGroup canvasGroup;
    private HUD hud;
    private PauseUI pauseUI;

    public InventoryItem Item
    {
        set
        {
            item = value;
            OnValidate();
        }
    }

    private void Awake()
    {
        pauseUI = FindObjectOfType<PauseUI>();
        hud = FindObjectOfType<HUD>();
        canvasGroup = GetComponent<CanvasGroup>();
        canvasGroup.alpha = 0;
    }

    private void OnValidate()
    {
        if (item != null)
        {
            itemImage.sprite = item.image;
            titleText.text = item.title;
            itemText.text = item.text;
        }
    }

    public void Show()
    {
        FadeIn();

        StartCoroutine(pauseUI.AwaitPause(fadeSpeed));
    }

    public void Continue()
    {
        pauseUI.Unpause();
        FadeOut();
        Destroy(gameObject, fadeSpeed);
    }

    private void FadeIn()
    {
        hud.FadeOut();
        canvasGroup.DOFade(1, fadeSpeed);
    }

    private void FadeOut()
    {
        hud.FadeIn();
        canvasGroup.DOFade(0, fadeSpeed);
    }
}