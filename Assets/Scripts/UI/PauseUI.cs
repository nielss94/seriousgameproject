﻿using System;
using System.Collections;
using System.Collections.Generic;
using DG.Tweening;
using UnityEngine;

public class PauseUI : MonoBehaviour
{
    [SerializeField] private float fadeSpeed = .5f;

    [SerializeField] private GameObject continueButton = null;
    [SerializeField] private GameObject bigPause = null;

    [SerializeField] private GameObject[] objectsToDisable = null;

    private CanvasGroup canvasGroup;
    private HUD hud;

    private void Awake()
    {
        hud = FindObjectOfType<HUD>();
        canvasGroup = GetComponent<CanvasGroup>();

        canvasGroup.alpha = 0;
    }

    public void Show()
    {
        ActivateObjects();
        FadeIn();

        StartCoroutine(AwaitPause(fadeSpeed));
    }

    public void Hide()
    {
        Unpause();
        FadeOut();

        StartCoroutine(AwaitDeactivateObjects(fadeSpeed));
    }

    private void ActivateObjects()
    {
        continueButton.SetActive(true);
        bigPause.SetActive(true);

        foreach (GameObject objectToDisable in objectsToDisable)
        {
            objectToDisable.SetActive(false);
        }
    }

    private void DeactivateObjects()
    {
        continueButton.SetActive(false);
        bigPause.SetActive(false);

        foreach (GameObject objectToDisable in objectsToDisable)
        {
            objectToDisable.SetActive(true);
        }
    }

    private IEnumerator AwaitDeactivateObjects(float waitTime)
    {
        yield return new WaitForSeconds(waitTime);

        DeactivateObjects();
    }

    public void Pause()
    {
        Time.timeScale = 0;
    }

    public void Unpause()
    {
        Time.timeScale = 1f;
    }

    public IEnumerator AwaitPause(float waitTime)
    {
        yield return new WaitForSeconds(waitTime);

        Pause();
    }

    private void FadeIn()
    {
        hud.FadeOut();
        canvasGroup.DOFade(1, fadeSpeed);
    }

    private void FadeOut()
    {
        hud.FadeIn();
        canvasGroup.DOFade(0, fadeSpeed);
    }
}