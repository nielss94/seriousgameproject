﻿using System;
using System.Collections;
using System.Collections.Generic;
using DG.Tweening;
using UnityEngine;

public class HUD : MonoBehaviour
{
    [SerializeField] private float fadeSpeed = .5f;

    private CanvasGroup canvasGroup;

    private void Awake()
    {
        canvasGroup = GetComponent<CanvasGroup>();
    }

    public void FadeIn()
    {
        canvasGroup.DOFade(1, fadeSpeed);
    }

    public void FadeOut()
    {
        canvasGroup.DOFade(0, fadeSpeed);
    }
}