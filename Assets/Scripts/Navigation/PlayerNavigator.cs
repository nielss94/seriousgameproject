﻿using UnityEngine;
using Cinemachine;

public class PlayerNavigator : MonoBehaviour
{
    private CinemachineDollyCart dolly;

    public Animator animator;
    public float speed;
    public float animSpeed;

    // Event to execute when player has the timing right
    public delegate void EndOfTrackReached();
    public event EndOfTrackReached OnEndOfTrackReachedEvent;

    void Start()
    {
        dolly = GetComponent<CinemachineDollyCart>();
        animator.speed = animSpeed;
    }

    void Update()
    {
        if (dolly && dolly.m_Position >= dolly.m_Path.PathLength)
        {
            // Disable animation
            animator.SetBool("Walking", false);

            // Fade to next scene.
            OnEndOfTrackReachedEvent();
        }
    }

    public void StartMoving()
    {
        // Enable animation
        animator.SetBool("Walking", true);

        if (dolly)
        {
            dolly.m_Speed = speed;
        }
    }

    public void StopPlayerSpeed() 
    {
        // Disable animation
        animator.SetBool("Walking", false);

        if (dolly) 
        {
            dolly.m_Speed = 0;
        }
    }

    private void OnDestroy()
    {
        OnEndOfTrackReachedEvent = delegate {};
    }
}
