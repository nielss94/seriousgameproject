﻿using UnityEngine;
using UnityEngine.SceneManagement;

public class GameManager : MonoBehaviour
{
    public static GameManager instance;
    private RhythmBall rhythmBall;
    private PlayerNavigator player;    
    
    // Event to execute when player has the timing right
    public delegate void SceneFinished();
    public event SceneFinished OnSceneFinishedEvent;

    private void Awake() 
    {    
        if(instance == null)
        {
            instance = this;
            DontDestroyOnLoad(this);
        }
        else
        {
            Destroy(gameObject);
            return;
        }

        SceneManager.sceneLoaded += OnSceneLoaded;
    }


    private void Start()
    {
        Initialize();
    }

    private void OnSceneLoaded(Scene scene, LoadSceneMode sceneMode)
    {   
        Initialize();
    }

    private void Initialize() 
    {
        rhythmBall = FindObjectOfType<RhythmBall>();
        player = FindObjectOfType<PlayerNavigator>();

        if(rhythmBall)
        {
            rhythmBall.OnPlayerInputLeftEvent += MovePlayer;
            rhythmBall.OnPlayerInputRightEvent += MovePlayer;

            rhythmBall.OnWrongPlayerInputLeftEvent += StopMovingPlayer;
            rhythmBall.OnWrongPlayerInputRightEvent += StopMovingPlayer;

            rhythmBall.OnNoPlayerInputEvent += StopMovingPlayer;
        }   
        if(player != null)
        {
            player.OnEndOfTrackReachedEvent += OnSceneFinished;
        }
    }

    private void OnSceneFinished()
    {
        OnSceneFinishedEvent?.Invoke();
    }

    private void MovePlayer()
    {
        player.StartMoving();
    }

    private void StopMovingPlayer()
    {
        player.StopPlayerSpeed();
    }

    public void TogglePause()
    {
        Time.timeScale = (Time.timeScale == 1f) ? 0f : 1f;
    }
}