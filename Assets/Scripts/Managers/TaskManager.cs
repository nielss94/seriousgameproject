using UnityEngine;
using System.Collections.Generic;
using System;
using UnityEngine.SceneManagement;
using System.Linq;

public class TaskManager : MonoBehaviour
{
    public event Action<Task, int, int> OnTaskChanged;

    [SerializeField] private List<Task> tasks = new List<Task>();

    [SerializeField] private Task currentTask;

    [SerializeField] private string[] prefixes = null;

    private static TaskManager instance;
    private FinalTask finalTask;

    private void Start()
    {
        SceneManager.sceneLoaded += OnSceneLoaded;
        //Invoke("Initialize", .1f);
        Initialize();
    }

    public void CompleteCurrentTask()
    {
        currentTask.Complete();
        SetCurrentTask();
    }

    private void FindDialogsAndSetTasks()
    {
        tasks.Clear();
        currentTask = null;
        var dialogs = FindObjectsOfType<Dialog>().OrderBy(d => d.transform.GetSiblingIndex()).ToArray();

        foreach (var dialog in dialogs)
        {
            int random = UnityEngine.Random.Range(0, prefixes.Length);
            string randomPrefix = prefixes[random];
            tasks.Add(new Task(randomPrefix + " " + dialog.name + "."));
        }

        if (finalTask != null)
        {
            tasks.Add(finalTask.Task);
        }
    }

    private void Initialize() 
    {
        finalTask = FindObjectOfType<FinalTask>();
        FindDialogsAndSetTasks();
        SetCurrentTask();
    }

    private void OnSceneLoaded(Scene scene, LoadSceneMode sceneMode)
    {
        Initialize();
    }

    private void SetCurrentTask()
    {
        foreach (var task in tasks)
        {
            if (!task.Completed)
            {
                currentTask = task;
                int currentIndex = tasks.IndexOf(currentTask) + 1;
                OnTaskChanged?.Invoke(currentTask, currentIndex, tasks.Count);
                return;
            }
        }

        currentTask = null;
        OnTaskChanged?.Invoke(currentTask, tasks.Count, tasks.Count);
    }
}