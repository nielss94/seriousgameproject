using System.Collections.Generic;
using UnityEngine;
using System.Linq;
using System;

public class InventoryManager : MonoBehaviour
{
    public static InventoryManager instance;

    [SerializeField] private List<InventoryItem> inventory = new List<InventoryItem>();
    [SerializeField] private InventoryItemReadWindow readWindowPrefab = null;
    public event Action<InventoryItem> OnInventoryChanged;

    private void Awake()
    {
        if (instance == null)
        {
            instance = this;
            DontDestroyOnLoad(this);
        }
        else
        {
            Destroy(gameObject);
            return;
        }
    }

    public void AddItem(InventoryItem item)
    {
        inventory.Add(item);
        OnInventoryChanged?.Invoke(item);
    }

    public IEnumerable<InventoryItem> GetTools()
    {
        return inventory.Where(item => item.type == ItemTypes.Tool);
    }

    public IEnumerable<InventoryItem> GetNotes()
    {
        return inventory.Where(item => item.type == ItemTypes.Note);
    }

    public void ShowInventoryRead(InventoryItem item)
    {
        InventoryItemReadWindow window = Instantiate(readWindowPrefab, FindObjectOfType<Canvas>().transform);
        window.Item = item;
        window.Show();
    }
}