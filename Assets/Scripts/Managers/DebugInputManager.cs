﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DebugInputManager : MonoBehaviour
{
    public event Action OnSuccessInputLeft;
    public event Action OnFailInputLeft;

    public event Action OnSuccessInputRight;
    public event Action OnFailInputRight;

    private void Update()
    {
        if (Debug.isDebugBuild)
        {
            if (Input.GetKeyDown(KeyCode.Z))
            {
                OnSuccessInputLeft?.Invoke();
            }

            if (Input.GetKeyDown(KeyCode.X))
            {
                OnFailInputLeft?.Invoke();
            }

            if (Input.GetKeyDown(KeyCode.M))
            {
                OnSuccessInputRight?.Invoke();
            }

            if (Input.GetKeyDown(KeyCode.N))
            {
                OnFailInputRight?.Invoke();
            }
        }
    }
}