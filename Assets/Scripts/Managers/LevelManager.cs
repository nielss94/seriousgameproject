﻿using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using System.Collections;
using DG.Tweening;

public class LevelManager : MonoBehaviour
{
    // The animator
    private Fader fader;

    // Start is called before the first frame update
    void Start()
    {
        FadeIn();
    }
    void FadeIn() 
    {
        fader = FindObjectOfType<Fader>();
        if(fader != null)
        {
            fader.levelManager = this;
            fader.FadeIn();
        }

        Invoke("EnsureFaded", 2);
    }

    private void EnsureFaded()
    {
        fader?.GetComponent<Image>()?.DOFade(0,.2f);
    }
    
    // Load main menu
    public void LoadMainMenu()
    {
        SceneManager.LoadScene(0);
    }

    // Load next level in order
    public void LoadNextLevel()
    {
        SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex+1);
    }

    public void LoadNextLevelInSeconds(float seconds)
    {
        StartCoroutine(WaitAndLoadLevel(seconds));
    }

    private IEnumerator WaitAndLoadLevel(float seconds)
    {
        yield return new WaitForSeconds(seconds);
        LoadNextLevel();
    }

}
