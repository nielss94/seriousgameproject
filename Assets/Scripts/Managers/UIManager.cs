﻿using System;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class UIManager : MonoBehaviour
{
    [Header("Metronome"), SerializeField] private GameObject leftWave = null;
    [SerializeField] private GameObject rightWave = null;
    [SerializeField] private GameObject leftCross = null;
    [SerializeField] private GameObject rightCross = null;
    [SerializeField] private RectTransform rightIndicator = null;
    [SerializeField] private RectTransform leftIndicator = null;
    [SerializeField] private float activeXPosition = 10;
    private float neutralXPosition;
    private RhythmBall rhythmBall;

    private void Awake()
    {
        rhythmBall = FindObjectOfType<RhythmBall>();
        neutralXPosition = rightIndicator.position.x;

        if (rhythmBall)
            rhythmBall.OnActivateMedicineEvent += MoveIndicatorsInside;
    }

    private void Start()
    {
        if(rhythmBall)
        {
            rhythmBall.OnPlayerInputLeftEvent += SuccessLeft;
            rhythmBall.OnPlayerInputRightEvent += SuccessRight;

            rhythmBall.OnWrongPlayerInputLeftEvent += FailLeft;
            rhythmBall.OnWrongPlayerInputRightEvent += FailRight;

            rhythmBall.OnDeactivateMedicineEvent += MoveIndicatorsOutside;
        }
    }

    private void SuccessLeft()
    {
        ReactivateGameObject(leftWave);
    }

    private void SuccessRight()
    {
        ReactivateGameObject(rightWave);
    }

    private void FailLeft()
    {
        ReactivateGameObject(leftCross);
    }

    private void FailRight()
    {
        ReactivateGameObject(rightCross);
    }

    private void MoveIndicatorsInside()
    {
        leftIndicator.localPosition = new Vector2(-activeXPosition, 0);
        rightIndicator.localPosition = new Vector2(activeXPosition, 0);
    }

    private void MoveIndicatorsOutside()
    {
        leftIndicator.localPosition = new Vector2(-neutralXPosition, 0);
        rightIndicator.localPosition = new Vector2(neutralXPosition, 0);
    }

    private static void ReactivateGameObject(GameObject uiObject)
    {
        uiObject.SetActive(false);
        uiObject.SetActive(true);
    }
}