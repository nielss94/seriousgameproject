﻿using UnityEngine;

public class LinkLevelManager : MonoBehaviour
{
    LevelManager levelManager;

    // Start is called before the first frame update
    void Start()
    {
        levelManager = FindObjectOfType<LevelManager>();
    }

    // Function to call the LoadMainMenu function in the levelmanager
    void LoadMainMenu()
    {
        levelManager.LoadMainMenu();
    }
}
