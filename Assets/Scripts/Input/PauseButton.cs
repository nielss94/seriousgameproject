﻿using UnityEngine;
using UnityEngine.UI;

public class PauseButton : MonoBehaviour
{
    [SerializeField] private PauseUI pauseUI = null;

    private bool paused = false;

    public void TogglePause()
    {
        paused = !paused;

        if (paused)
        {
            pauseUI.Show();
        }
        else
        {
            pauseUI.Hide();
        }
    }
}