﻿using System;
using UnityEngine;
using UnityEngine.UI;
using DG.Tweening;

public class RhythmBall : MonoBehaviour
{
    [Header("Move positions"), SerializeField]
    private GameObject leftDestination = null;

    [SerializeField] private GameObject rightDestination = null;

    [Header("Speed"), SerializeField] private float moveSpeed = 200f;

    private bool moveRight = true;

    private RectTransform rightDestinationRectTransform;
    private RectTransform leftDestinationRectTransform;
    private RectTransform rectTransform;

    [SerializeField] private float transparencyFactor = 0.1f;
    private Image image = null;

    private AudioSource audioSource;
    [SerializeField] private AudioClip metronomeSound = null;
    [SerializeField] private AudioClip correctInputSound = null;
    [SerializeField] private AudioClip incorrectInputSound = null;

    private bool audioQueue = false;

    // Window durin which the player can press button
    [SerializeField] private float window = 0f;

    // Event to execute when player has the timing right
    public event Action OnPlayerInputLeftEvent = delegate { };
    public event Action OnPlayerInputRightEvent = delegate { };
    public event Action OnWrongPlayerInputLeftEvent = delegate { };
    public event Action OnWrongPlayerInputRightEvent = delegate { };
    public event Action OnNoPlayerInputEvent = delegate { };

    public event Action OnActivateMedicineEvent = delegate { };
    public event Action OnDeactivateMedicineEvent = delegate { };

    [SerializeField] private float rhythmAdoptation = 0.0f;

    // The clickable areas
    [SerializeField] private ClickableArea leftClick = null;
    [SerializeField] private ClickableArea rightClick = null;

    // Current area number
    private int currentArea;

    [SerializeField] private CanvasGroup rhythmBarCanvasGroup;

    private bool clicked = false;

    private bool executedCheck = false;

    [SerializeField] private bool medicine = false;
    [SerializeField] private bool metronome = false;

    private void Awake()
    {
        rhythmBarCanvasGroup = GetComponentInParent<CanvasGroup>();
        audioSource = this.GetComponent<AudioSource>();

        rectTransform = this.GetComponent<RectTransform>();
        rightDestinationRectTransform = rightDestination.GetComponent<RectTransform>();
        leftDestinationRectTransform = leftDestination.GetComponent<RectTransform>();
    }

    private void Start()
    {
        leftClick.OnClickEvent += delegate
        {
            IsSuccessClick(leftDestinationRectTransform.position, GetWindow(), true);
        };
        rightClick.OnClickEvent += delegate
        {
            IsSuccessClick(rightDestinationRectTransform.position, GetWindow(), false);
        };

        image = this.GetComponent<Image>();
        rectTransform.position = leftDestinationRectTransform.position;

        if (metronome)
            ToggleAudio();

        if (medicine)
            ActivateMedicine();
    }

    public void Activate()
    {
        gameObject.SetActive(true);
    }

    private void Update()
    {
        if (IsOnPosition(leftDestinationRectTransform.position))
        {
            if (audioQueue)
                audioSource.PlayOneShot(metronomeSound);

            executedCheck = false;
            moveRight = true;
        }

        if (IsOnPosition(rightDestinationRectTransform.position))
        {
            if (audioQueue)
                audioSource.PlayOneShot(metronomeSound);

            executedCheck = false;
            moveRight = false;
        }

        if (moveRight)
        {
            rectTransform.position = Vector3.MoveTowards(rectTransform.position, rightDestinationRectTransform.position,
                moveSpeed * Time.deltaTime);
        }

        if (!moveRight)
        {
            rectTransform.position = Vector3.MoveTowards(rectTransform.position, leftDestinationRectTransform.position,
                moveSpeed * Time.deltaTime);
        }

        HasClicked();
    }

    private void HasClicked()
    {
        if (!executedCheck)
        {
            if (moveRight && leftDestinationRectTransform.position.x + window < rectTransform.position.x)
            {
                if (!clicked)
                    OnNoPlayerInputEvent.Invoke();

                executedCheck = true;
                clicked = false;
            }
            else if (!moveRight && rightDestinationRectTransform.position.x - window > rectTransform.position.x)
            {
                if (!clicked)
                    OnNoPlayerInputEvent.Invoke();

                executedCheck = true;
                clicked = false;
            }
        }
    }

    private bool IsOnPosition(Vector3 position)
    {
        float distance = Vector3.Distance(rectTransform.position, position);
        return distance < 0.1f;
    }

    private void IsSuccessClick(Vector3 position, float distance, bool leftClick)
    {
        clicked = true;

        bool success = Vector3.Distance(rectTransform.position, position) < distance;

        if (success)
        {
            LowerAlpha();
            audioSource.PlayOneShot(correctInputSound);

            if (leftClick)
                OnPlayerInputLeftEvent.Invoke();
            else
                OnPlayerInputRightEvent.Invoke();
        }
        else
        {
            ResetAlpha();
            audioSource.PlayOneShot(incorrectInputSound);

            if (leftClick)
                OnWrongPlayerInputLeftEvent.Invoke();
            else
                OnWrongPlayerInputRightEvent.Invoke();
        }
    }

    public void LowerAlpha()
    {
        float alpha = image.color.a;
        alpha = (alpha - transparencyFactor > 0) ? alpha - transparencyFactor : 0;

        image.color = new Color(image.color.r, image.color.g, image.color.b, alpha);
    }

    public void ResetAlpha()
    {
        image.color = new Color(image.color.r, image.color.g, image.color.b, 1);
    }

    public void ToggleAudio()
    {
        audioQueue = !audioQueue;
    }

    public void FadeAudioOut()
    {
        if (audioQueue)
            audioSource.volume = 0;
    }
    public void FadeAudioIn()
    {
        if (audioQueue)
            audioSource.volume = 1;
    }

    public void UpdateWindow(float addition)
    {
        window += addition;
    }

    private float GetWindow()
    {
        return window;
    }

    public void ActivateMedicine()
    {
        UpdateWindow(rhythmAdoptation);
        OnActivateMedicineEvent.Invoke();
    }

    public void DeactivateMedicine()
    {
        UpdateWindow(-rhythmAdoptation);
        OnDeactivateMedicineEvent.Invoke();
    }
}