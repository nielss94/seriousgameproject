﻿using System;
using System.Collections;
using DG.Tweening;
using UnityEngine;
using UnityEngine.UI;

public class ClickableArea : MonoBehaviour
{
    [SerializeField] private bool tutorialHighlight = false;
    [SerializeField] private float fadeSpeed = 2f;
    [SerializeField] private Color fullColor = Color.yellow;
    [SerializeField] private GameObject clickHereGroup = null;

    private CanvasGroup clickHereCanvas;
    private Color transparentColor;
    private Image image;

    public event Action OnClickEvent;

    private void Awake()
    {
        clickHereCanvas = clickHereGroup.GetComponent<CanvasGroup>();
        image = GetComponent<Image>();
    }

    private void Start()
    {
        transparentColor = new Color(fullColor.r, fullColor.g, fullColor.b, 0);
        image.color = transparentColor;

        clickHereCanvas.alpha = 0;

        if (tutorialHighlight)
        {
            StartCoroutine(ShowFade());
        }
    }

    private IEnumerator ShowFade()
    {
        Sequence colorSequence = DOTween.Sequence();

        colorSequence.Append(image.DOColor(fullColor, fadeSpeed));
        colorSequence.Append(image.DOColor(transparentColor, fadeSpeed));

        Sequence textSequence = DOTween.Sequence();

        textSequence.Append(clickHereCanvas.DOFade(1, fadeSpeed));
        textSequence.Append(clickHereCanvas.DOFade(0, fadeSpeed));

        colorSequence.Play();
        textSequence.Play();

        if (tutorialHighlight)
        {
            yield return new WaitForSeconds(fadeSpeed * 2);
            StartCoroutine(ShowFade());
        }
    }

    // Check player input
    public void OnClick()
    {
        if (tutorialHighlight)
        {
            tutorialHighlight = false;
        }

        OnClickEvent?.Invoke();
    }
}