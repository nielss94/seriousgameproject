﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DialogManager : MonoBehaviour
{
    [SerializeField] private Dialog[] dialogs;
    [SerializeField] private DialogWindow dialogWindowPrefab = null;
    private Canvas canvas;

    private AudioSource audioSource;
    [SerializeField] private AudioClip receiveNoteClip = null;

    private void Start()
    {
        audioSource = GetComponent<AudioSource>();
        canvas = FindObjectOfType<Canvas>();

        FindAllDialogs();
        SubscribeToAllDialogs();
    }

    public void ShowDialog(Sprite sprite, string text, List<InventoryItem> items)
    {
        audioSource.PlayOneShot(receiveNoteClip);
        DialogWindow window = Instantiate(dialogWindowPrefab, canvas.transform);

        window.image.sprite = sprite;
        window.dialogText.text = text;
        window.Items = items;
    }

    private void SubscribeToAllDialogs() 
    {
        foreach (var dialog in dialogs)
        {
            dialog.OnStartDialog += ShowDialog;
        }
    }

    private void FindAllDialogs() 
    {
        dialogs = FindObjectsOfType<Dialog>();
    }
    
}
