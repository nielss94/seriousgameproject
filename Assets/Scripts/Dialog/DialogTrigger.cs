﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Cinemachine;

public class DialogTrigger : MonoBehaviour
{
    [SerializeField] private Dialog dialog = null;

    private void OnValidate() {
        dialog = transform.parent?.GetComponent<Dialog>();
    }

    private void OnTriggerEnter(Collider other) {
        if(other.CompareTag("Player"))
        {
            dialog?.StartDialog();
            FindObjectOfType<PlayerNavigator>().StopPlayerSpeed();
            FindObjectOfType<RhythmBall>().ResetAlpha();
            FindObjectOfType<TaskManager>().CompleteCurrentTask();
        }
    }
}
