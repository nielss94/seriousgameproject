﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class InstantDialog : MonoBehaviour
{
    [SerializeField]private Dialog dialog = null;
    DialogWindow window;
    private void Start() 
    {
        Invoke("StartDialog", 1.5f);  
    }

    private void StartDialog()
    {
        dialog?.StartDialog(); 
        window = FindObjectOfType<DialogWindow>();

        window.OnWindowClosed += OnDialogClose;
    }

    private void OnDialogClose() 
    {
        StartCoroutine(WaitAndEndScene());
    }

    IEnumerator WaitAndEndScene()
    {
        yield return new WaitForSeconds(2f);
        FindObjectOfType<Fader>().FadeOut();
        FindObjectOfType<LevelManager>().LoadNextLevelInSeconds(2);
    }
}
