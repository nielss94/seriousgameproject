using System.Collections.Generic;
using UnityEngine;
using System;

public class Dialog : MonoBehaviour {
    public new string name;
    public Sprite sprite;
    [TextArea]public string text;
    public List<InventoryItem> items = new List<InventoryItem>();

    public event Action<Sprite, string, List<InventoryItem>> OnStartDialog;

    public void StartDialog() 
    {
        OnStartDialog?.Invoke(sprite, text, items);
    }
    
}