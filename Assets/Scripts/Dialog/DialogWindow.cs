using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;
using DG.Tweening;
using System;

public class DialogWindow : MonoBehaviour
{
    public event Action OnWindowClosed = delegate{};
    public Image image;
    public TextMeshProUGUI title;
    public TextMeshProUGUI dialogText;
    public TextMeshProUGUI itemText;
    public Button continueButton;
    private CanvasGroup canvasGroup;
    private DialogManager dialogManager;
    private HUD hud;
    private RhythmBall rhythmBall;

    [SerializeField] private float fadeSpeed = .5f;
    [SerializeField] private InventoryItem item;
    [SerializeField] private List<InventoryItem> items = new List<InventoryItem>();

    public InventoryItem Item
    {
        get { return item; }
        set
        {
            item = value;
            if (item != null)
            {
                title.text = item.title;
                itemText.text = item.text;
            }
        }
    }

    public List<InventoryItem> Items
    {
        get { return items; }
        set
        {
            items = value;
            if (items.Count > 0)
            {
                Item = items[0];
            }
        }
    }

    private void Awake()
    {
        hud = FindObjectOfType<HUD>();
        canvasGroup = GetComponent<CanvasGroup>();
        dialogManager = FindObjectOfType<DialogManager>();
        rhythmBall = FindObjectOfType<RhythmBall>();
        canvasGroup.alpha = 0;
    }

    private void Start()
    {
        FadeIn();
    }

    public void Continue()
    {
        InventoryManager.instance.AddItem(item);

        items.Remove(item);

        if (items.Count > 0)
        {
            dialogManager.ShowDialog(image.sprite, dialogText.text, items);
        }
        else
        {
            hud.FadeIn();
        }

        FadeOut();
        OnWindowClosed();
        Destroy(gameObject, fadeSpeed);
    }

    private void FadeIn()
    {
        hud.FadeOut();
        canvasGroup.DOFade(1, fadeSpeed);
        rhythmBall?.FadeAudioOut();
    }

    private void FadeOut()
    {
        canvasGroup.DOFade(0, fadeSpeed);
        rhythmBall?.FadeAudioIn();
    }
}