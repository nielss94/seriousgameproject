﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LoadNextLevelInSeconds : MonoBehaviour
{
    public float seconds;

    private void Start() {
        FindObjectOfType<LevelManager>().LoadNextLevelInSeconds(seconds);
        Invoke("FadeOut", seconds - 1);
    }

    private void FadeOut() 
    {
        FindObjectOfType<Fader>().FadeOut();
    }
}
