﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RepeatingBG : MonoBehaviour
{
    public int startZ = 340; // background 340 | foreground 60
    public int endZ = -210; // background -210 | foreground -80
    private Vector3 origin;
    private Vector3 destination;

    public float speed; // will be influenced by the player speed

    void Start()
    {
        origin = new Vector3(transform.position.x, transform.position.y, startZ);
        destination = new Vector3(transform.position.x, transform.position.y, endZ);
    }
    
    void Update()
    { 
        MoveBackground(speed);
    }

    void MoveBackground(float speed)
    {
        transform.position = Vector3.MoveTowards(transform.position, destination, speed * Time.deltaTime);

        if (transform.position.z <= endZ + 10) // + 10 to make sure components move through boundary.
        {
            transform.position = origin;
        }
    }
}
