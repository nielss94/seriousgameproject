﻿using UnityEngine;
using UnityEngine.UI;
using DG.Tweening;

public class Fader : MonoBehaviour
{
    // The level manager
    public LevelManager levelManager;

    private void Start() {
        if(GameManager.instance != null)
        {
            GameManager.instance.OnSceneFinishedEvent += FadeOut;
        }
    }

    // Play the fadeout animation
    public void FadeOut()
    {
        GetComponent<Animator>().SetTrigger("FadeOut");
    }

    // Play the fadein animation
    public void FadeIn()
    {
        GetComponent<Animator>().SetTrigger("FadeIn");
    }

    // Call transition to new level in the levelManager
    public void LoadNextLevel()
    {
        levelManager.LoadNextLevel();
    }

    private void OnDestroy() {
        GameManager.instance.OnSceneFinishedEvent -= FadeOut;
    }
}
