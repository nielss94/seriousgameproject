﻿using System;
using System.Collections;
using System.Collections.Generic;
using DG.Tweening;
using UnityEngine;

[RequireComponent(typeof(RectTransform))]
public class BounceUIAnimation : MonoBehaviour
{
    [SerializeField] private float animationTime = 1f;
    [SerializeField] private GameObject destination = null;

    [Header("Automatic hide"), SerializeField]
    private bool hideAfterSeconds = false;

    [SerializeField] private float shownTime = 2f;

    private RectTransform rectTransform;
    private RectTransform destinationRectTransform;
    private Vector3 originalPosition;

    private void Awake()
    {
        rectTransform = GetComponent<RectTransform>();
        destinationRectTransform = destination.GetComponent<RectTransform>();
    }

    private void Start()
    {
        originalPosition = rectTransform.position;
    }

    public void InAnimation()
    {
        rectTransform.DOMove(destinationRectTransform.position, animationTime).SetEase(Ease.OutBounce);

        if (hideAfterSeconds)
        {
            StartCoroutine(AwaitOutAnimation());
        }
    }

    private IEnumerator AwaitOutAnimation()
    {
        yield return new WaitForSeconds(shownTime);

        OutAnimation();
    }

    public void OutAnimation()
    {
        rectTransform.DOMove(originalPosition, animationTime);
    }

}