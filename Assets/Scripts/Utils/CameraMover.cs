﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraMover : MonoBehaviour
{
    private int endZ = 6;
    private Vector3 destination;
    public int speed;

    void Start()
    {
        destination = new Vector3(transform.position.x, transform.position.y, endZ);
    }
    
    void Update()
    {
        if (transform.position.z < endZ)
        {
            transform.position = Vector3.MoveTowards(transform.position, destination, speed * Time.deltaTime);
        }
    }
}
