﻿using UnityEngine;

public class PersistGameObject : MonoBehaviour
{
    // Awake is called once the object is intantiated
    void Awake()
    {
        GameObject[] gameObjects = GameObject.FindGameObjectsWithTag(this.gameObject.tag);
        Debug.Log(gameObjects.Length);
        if (gameObjects.Length > 1)
            Destroy(this.gameObject);

        DontDestroyOnLoad(this.gameObject);
    }
}
