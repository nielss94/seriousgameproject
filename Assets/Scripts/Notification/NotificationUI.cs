using System;
using System.Collections;
using System.Collections.Generic;
using DG.Tweening;
using TMPro;
using UnityEngine;

[RequireComponent(typeof(BounceUIAnimation))]
public class NotificationUI : MonoBehaviour
{
    [Header("Text"), SerializeField] private TextMeshProUGUI notificationText = null;

    private BounceUIAnimation bounceUIAnimation;

    private void Awake()
    {
        bounceUIAnimation = GetComponent<BounceUIAnimation>();
    }

    private void Start()
    {
        InventoryManager.instance.OnInventoryChanged += ShowNotification;
    }


    private void ShowNotification(InventoryItem item)
    {
        SetText(item.title);
        bounceUIAnimation.InAnimation();
    }

    private void SetText(string itemName)
    {
        notificationText.text = "\"" + itemName + "\" ontvangen";
    }

    private void OnDestroy()
    {
        InventoryManager.instance.OnInventoryChanged -= ShowNotification;
    }
}