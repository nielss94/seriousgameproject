using UnityEngine;

[CreateAssetMenu(menuName="InventoryItem")]
public class InventoryItem : ScriptableObject
{
    public string title;
    public Sprite image;
    public ItemTypes type;
    [TextArea(0, 25)]
    public string text;
}

public enum ItemTypes {
    Note,
    Tool
}