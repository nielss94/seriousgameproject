﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(BoxCollider))]
public class FinalTask : MonoBehaviour
{
    [SerializeField] private Task task = null;

    public Task Task
    {
        get => task;
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag("Player"))
        {
            FindObjectOfType<TaskManager>().CompleteCurrentTask();
        }
    }
}