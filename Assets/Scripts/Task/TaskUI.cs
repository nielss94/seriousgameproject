﻿using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

[RequireComponent(typeof(BounceUIAnimation))]
public class TaskUI : MonoBehaviour
{
    [SerializeField] private TextMeshProUGUI taskText = null;
    [SerializeField] private TextMeshProUGUI taskCountText = null;

    private bool inCanvas = false;

    private BounceUIAnimation bounceUIAnimation;
    private TaskManager taskManager;

    private void Awake()
    {
        bounceUIAnimation = GetComponent<BounceUIAnimation>();
        taskManager = FindObjectOfType<TaskManager>();
        ClearTask();
        taskManager.OnTaskChanged += HandleTaskChange;
    }

    private void HandleTaskChange(Task task, int currentTask, int numberOfTasks)
    {
        if (!inCanvas)
        {
            bounceUIAnimation.InAnimation();
            inCanvas = true;
        }

        if (task == null)
        {
            ClearTask();

            if (inCanvas)
            {
                bounceUIAnimation.OutAnimation();
                inCanvas = false;
            }
        }
        else
        {
            SetTaskText(task.Text);
            SetTaskCountText(currentTask, numberOfTasks);
        }
    }

    private void ClearTask()
    {
        SetTaskText("");
        SetTaskCountText("");
    }

    private void SetTaskText(string text)
    {
        taskText.text = text;
    }

    private void SetTaskCountText(int currentTask, int numberOfTasks)
    {
        SetTaskCountText(currentTask + "/" + numberOfTasks);
    }

    private void SetTaskCountText(string text)
    {
        taskCountText.text = text;
    }

    private void OnDestroy() 
    {
        taskManager.OnTaskChanged -= HandleTaskChange;
    }
}