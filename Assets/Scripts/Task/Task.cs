using UnityEngine;

[System.Serializable]
public class Task
{
    [SerializeField, TextArea]private string text = null;
    public string Text => text;

    [SerializeField]private bool completed;
    public bool Completed { 
        get {
            return completed;
        } 
        private set{ 
            completed = value;
        } 
    }

    public Task(string text, bool completed = false)
    {
        this.text = text;
        this.completed = completed;
    }

    public void Complete()
    {
        completed = true;
    }
}