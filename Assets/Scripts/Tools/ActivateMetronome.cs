﻿using UnityEngine;

public class ActivateMetronome : MonoBehaviour
{
    private RhythmBall rhythmBall;

    // Start is called before the first frame update
    void Start()
    {
        rhythmBall = FindObjectOfType<RhythmBall>();
    }

    private void OnTriggerEnter(Collider other)
    {
        rhythmBall.ToggleAudio();
    }
}
